#!/usr/bin/env python

import os
from time import sleep
import time
import RPi.GPIO as GPIO
import xml.etree.ElementTree as ET
from subprocess import call

GPIO.setmode(GPIO.BCM)
tree = ET.parse('settings.xml')
root = tree.getroot()
sound_clips = []
gpio_setting = []
sound_length = []
sound_counter = []
switch_normal = []
for child in root:
    for gc in child:
        for gcc in gc:
            if 'gpio' in gcc.tag.lower():
                gpio_setting.append(gcc.text)
            if 'audio' in gcc.tag.lower():
                sound_clips.append(gcc.text)
            if 'length' in gcc.tag.lower():
                sound_length.append(float(gcc.text))
                sound_counter.append(0)
            if 'switch' in gcc.tag.lower():
                if(gcc.text == 'no'):
                    switch_normal.append(False)
                else:
                    switch_normal.append(True)

print(gpio_setting)
print(sound_clips)

for i in range(len(gpio_setting)):
    print("GPIO.setup("+gpio_setting[i]+", GPIO.IN)")
    GPIO.setup(int(gpio_setting[i]), GPIO.IN, pull_up_down=GPIO.PUD_UP)
    print("switch = " + str(switch_normal[i]))

while True:
    for i in range(len(gpio_setting)):
        if (GPIO.input(int(gpio_setting[i])) == switch_normal[i]):
            if (time.time() - sound_counter[i]) >= sound_length[i]:
                sound_counter[i] = time.time()
                os.system('mpg123 media/' + sound_clips[i] + ' &')
    sleep(0.2)
