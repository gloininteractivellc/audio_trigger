#!/bin/bash -e

sudo modprobe snd_bcm2835
sudo amixer cset numid=3 1
sudo apt-get -y install python-dev python-rpi.gpio
sudo pip install mutagen
