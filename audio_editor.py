from Tkinter import *
import tkFileDialog
from functools import partial
import os
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element, SubElement
from xml.dom import minidom
import sys

class AudioTrigger:
    def __init__(self):
        print("audio_editor started")
        self.top = None
        
    def edit_audio(self, evt):
        w = evt.widget
        self.index = int(w.curselection()[0])
        value = w.get(self.index)
        if self.top is None:
            self.top = Toplevel()
            self.top.title("Edit Audio")
            self.gpio_val = IntVar()
            self.gpio_val.set(self.trigger_gpio[self.index])
            self.gpio_setup()
            self.set_top_filename()
            self.filename_input.insert(END, self.trigger_audio[self.index])
            self.set_top_time()
            self.audio_time.insert(END, self.trigger_length[self.index])
            self.normal_val = StringVar()
            self.normal_val.set(self.trigger_normal[self.index])
            self.set_top_normal()
            save_button = Button(self.top, command=self.save_audio, text="Save").grid(row=20, column=0)
            delete_button = Button(self.top, command=self.delete_button, text="Delete").grid(row=20, column=1)
            cancel_button = Button(self.top, command=self.exit_audio, text="Cancel").grid(row=21, column=0, columnspan=2)
            self.top.protocol("WM_DELETE_WINDOW", self.exit_audio)
        
    def save_audio(self):
        self.save_settings()
        self.top.destroy()
        self.top = None

    def save_new_audio(self):
        if self.gpio_val.get() > 0 and len(self.filename_input.get()) > 0 and len(self.audio_time.get()) > 0:
           self.index = len(self.trigger_gpio)
           self.trigger_gpio.append(self.gpio_val.get())
           self.trigger_audio.append(self.filename_input.get())
           self.trigger_length.append(self.audio_time.get())
           self.audio_selector.insert(END, self.filename_input.get())
           self.save_settings()
           self.top.destroy()
           self.top = None
        else:
           self.warning_top =  Toplevel()
           self.warning_top.title("Error")
           Label(self.warning_top, text="Please select a GPIO pin, choose a filename, and enter a length before saving.").grid(row=1, column=0)
           cancel_button = Button(self.warning_top, command=self.close_warning, text="Cancel").grid(row=2, column=0)

    def delete_button(self):
        self.trigger_gpio.pop(self.index)
        self.trigger_audio.pop(self.index)
        self.trigger_length.pop(self.index)
        selection = self.audio_selector.curselection()
        self.audio_selector.delete(selection[0])
        self.write_xml()
        self.top.destroy()
        self.top = None

    def close_warning(self):
        self.warning_top.destroy()

    def save_settings(self):
        self.trigger_gpio[self.index] = self.gpio_val.get()
        self.trigger_audio[self.index] = self.filename_input.get()
        self.trigger_length[self.index] = self.audio_time.get()
        self.trigger_normal[self.index] = self.normal_val.get()
        self.write_xml()

    def write_xml(self):
        settings = Element("settings", urlid="http://www.microsoft.com/migration/1.0/migxmlext/audio_trigger_settings")
        title = SubElement(settings, "title")
        title.text = self.title_input.get()
        trigger_files = SubElement(settings, "trigger_files")
        for i, val in enumerate(self.trigger_audio):
            trigger_file = SubElement(trigger_files, "trigger_file")
            ET.SubElement(trigger_file, "gpio").text = str(self.trigger_gpio[i])
            ET.SubElement(trigger_file, "audio_file").text = val
            ET.SubElement(trigger_file, "length").text = str(self.trigger_length[i])
            ET.SubElement(trigger_file, "switch").text = str(self.trigger_normal[i])
        with open("settings.xml", "w") as f:
            f.write(self.prettify(settings))
    
    def exit_audio(self):
        self.top.destroy()
        self.top = None
        
    def open_file_handler(self):
        file_name = tkFileDialog.askopenfilename(initialdir="media", title="Select File", filetypes=(("MP3's", "*.mp3"), ("WAV's", "*.wav"), ("All FIles", "*.*")))
        file_name_split = file_name.split("/")
        self.filename_input.delete(0, END)
        self.filename_input.insert(0, file_name_split[len(file_name_split) - 1])

    def gpio_setup(self):
        gpio2 = Radiobutton(self.top, text="GPIO 2, Pin 3", variable=self.gpio_val, value=2)
        gpio2.grid(sticky="W", row=1, column=0)

        gpio3 = Radiobutton(self.top, text="GPIO 3, Pin 5", variable=self.gpio_val, value=3)
        gpio3.grid(sticky="W", row=1, column=1)

        gpio4 = Radiobutton(self.top, text="GPIO 4, Pin 7", variable=self.gpio_val, value=4)
        gpio4.grid(sticky="W", row=2, column=0)

        gpio14 = Radiobutton(self.top, text="GPIO 14, Pin 8", variable=self.gpio_val, value=14)
        gpio14.grid(sticky="W", row=2, column=1)

        gpio15 = Radiobutton(self.top, text="GPIO 15, Pin 10", variable=self.gpio_val, value=15)
        gpio15.grid(sticky="W", row=3, column=0)

        gpio17 = Radiobutton(self.top, text="GPIO 17, Pin 11", variable=self.gpio_val, value=17)
        gpio17.grid(sticky="W", row=3, column=1)

        gpio18 = Radiobutton(self.top, text="GPIO 18, Pin 12", variable=self.gpio_val, value=18)
        gpio18.grid(sticky="W", row=4, column=0)

        gpio27 = Radiobutton(self.top, text="GPIO 27, Pin 13", variable=self.gpio_val, value=27)
        gpio27.grid(sticky="W", row=4, column=1)

        gpio22 = Radiobutton(self.top, text="GPIO 22, Pin 15", variable=self.gpio_val, value=22)
        gpio22.grid(sticky="W", row=5, column=0)

        gpio23 = Radiobutton(self.top, text="GPIO 23, Pin 16", variable=self.gpio_val, value=23)
        gpio23.grid(sticky="W", row=5, column=1)

        gpio24 = Radiobutton(self.top, text="GPIO 24, Pin 18", variable=self.gpio_val, value=24)
        gpio24.grid(sticky="W", row=6, column=0)

        gpio10 = Radiobutton(self.top, text="GPIO 10, Pin 19", variable=self.gpio_val, value=10)
        gpio10.grid(sticky="W", row=6, column=1)

        gpio9 = Radiobutton(self.top, text="GPIO 9, Pin 21", variable=self.gpio_val, value=9)
        gpio9.grid(sticky="W", row=7, column=0)

        gpio25 = Radiobutton(self.top, text="GPIO 25, Pin 22", variable=self.gpio_val, value=25)
        gpio25.grid(sticky="W", row=7, column=1)

        gpio11 = Radiobutton(self.top, text="GPIO 11, Pin 23", variable=self.gpio_val, value=11)
        gpio11.grid(sticky="W", row=8, column=0)

        gpio8 = Radiobutton(self.top, text="GPIO 8, Pin 24", variable=self.gpio_val, value=8)
        gpio8.grid(sticky="W", row=8, column=1)

        gpio7 = Radiobutton(self.top, text="GPIO 7, Pin 26", variable=self.gpio_val, value=7)
        gpio7.grid(sticky="W", row=9, column=0)

        gpio5 = Radiobutton(self.top, text="GPIO 5, Pin 29", variable=self.gpio_val, value=5)
        gpio5.grid(sticky="W", row=9, column=1)

        gpio6 = Radiobutton(self.top, text="GPIO 6, Pin 31", variable=self.gpio_val, value=6)
        gpio6.grid(sticky="W", row=10, column=0)

        gpio12 = Radiobutton(self.top, text="GPIO 12, Pin 32", variable=self.gpio_val, value=12)
        gpio12.grid(sticky="W", row=10, column=1)

        gpio13 = Radiobutton(self.top, text="GPIO 13, Pin 33", variable=self.gpio_val, value=13)
        gpio13.grid(sticky="W", row=11, column=0)

        gpio19 = Radiobutton(self.top, text="GPIO 19, Pin 35", variable=self.gpio_val, value=19)
        gpio19.grid(sticky="W", row=11, column=1)

        gpio16 = Radiobutton(self.top, text="GPIO 16, Pin 36", variable=self.gpio_val, value=16)
        gpio16.grid(sticky="W", row=12, column=0)

        gpio26 = Radiobutton(self.top, text="GPIO 26, Pin 37", variable=self.gpio_val, value=26)
        gpio26.grid(sticky="W", row=12, column=1)

        gpio20 = Radiobutton(self.top, text="GPIO 20, Pin 38", variable=self.gpio_val, value=20)
        gpio20.grid(sticky="W", row=13, column=0)

        gpio21 = Radiobutton(self.top, text="GPIO 21, Pin 40", variable=self.gpio_val, value=21)
        gpio21.grid(sticky="W", row=13, column=1)

    def set_top_filename(self):
        Label(self.top, text="Audio File").grid(row=14, column=0)
        self.filename_input = Entry(self.top)
        self.filename_input.grid(row=14, column=1)
        file_button = Button(self.top, command=self.open_file_handler, text="Find Filename").grid(row=15, column=1)

    def set_top_time(self):
        Label(self.top, text="Audio Time").grid(row=16, column=0)
        self.audio_time = Entry(self.top)
        self.audio_time.grid(row=16, column=1)

    def set_top_normal(self):
        Label(self.top, text="Swtich Logic").grid(row=18, column=0, columnspan=2)
        normal = Radiobutton(self.top, text="Normally Open", variable=self.normal_val, value="no")
        normal.grid(sticky="W", row=19, column=0)
        not_normal = Radiobutton(self.top, text="Normally Closed", variable=self.normal_val, value="nc")
        not_normal.grid(sticky="W", row=19, column=1)

    def add_audio(self):
        if self.top is None:
            self.top = Toplevel()
            self.top.title("Add Audio")
            self.gpio_val = IntVar()
            self.gpio_setup()
            self.set_top_filename()
            self.set_top_time()
            save_button = Button(self.top, command=self.save_new_audio, text="Save").grid(row=17, column=0)
            cancel_button= Button(self.top, command=self.exit_audio, text="Cancel").grid(row=17, column=1)

    def save_trigger(self):
        self.write_xml()
        
    def exit_app(self):
        self.root.destroy()

    def main(self):
        tree = ET.parse('settings.xml')
        tree_root = tree.getroot()
        title = ""
        self.trigger_gpio = []
        self.trigger_audio = []
        self.trigger_length = []
        self.trigger_normal = []
        for child in tree_root:
            if "title" in child.tag:
                title = child.text
            for gc in child:
                for gcc in gc:
                    if "gpio" in gcc.tag:
                        self.trigger_gpio.append(gcc.text)
                    elif "audio_file" in gcc.tag:
                        self.trigger_audio.append(gcc.text)
                    elif "length" in gcc.tag:
                        self.trigger_length.append(gcc.text)
                    elif "switch" in gcc.tag:
                        self.trigger_normal.append(gcc.text)
        self.root = Tk(className="Audio Trigger Editor")
        Label(self.root, text="Audio Trigger Editor").grid(row=0, column=0, columnspan=2)
        Label(self.root, text="Title").grid(row=1, column=0)
        self.title_input = Entry(self.root)
        self.title_input.insert(END, title.strip())
        self.title_input.grid(row=1, column=1)
        Label(self.root, text="Select Audio").grid(row=2, column=0)
        self.audio_selector = Listbox(self.root)
        self.audio_selector.bind('<<ListboxSelect>>', self.edit_audio)
        for i in range(0, len(self.trigger_audio)):
            self.audio_selector.insert(END, self.trigger_audio[i])
        self.audio_selector.grid(row=2, column=1)
        add_new_button = Button(self.root, text="Add Audio", command=self.add_audio)
        add_new_button.grid(row=3, column=0)
        save_button = Button(self.root, text="Save", command=self.save_trigger)
        save_button.grid(row=3, column=1, stick="e")
        exit_button = Button(self.root, text="Exit", command=self.exit_app)
        exit_button.grid(row=4, column=0, columnspan=2)
        self.root.protocol("WM_DELETE_WINDOW", self.exit_app)
        self.root.mainloop()

    def prettify(self, elem):
        rough_string = ET.tostring(elem, 'utf-8')
        reparsed = minidom.parseString(rough_string)
        return reparsed.toprettyxml(indent="\t")

main = AudioTrigger()
main.main()
