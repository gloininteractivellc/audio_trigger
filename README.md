# Audio Trigger

Audio Trigger provides a quick, triggered audio player for Raspiberry PI, using the GPIO pins triggered to ground. This project was designed with Museum's and Kiosks in mind.

Sound clips (and the GPIO pins they are associated with) are defined in the settings.xml file.

This has only successfully been tested using MP3's. WAV's have been confirmed not to work (the base player is mpg123). If you need to convert or edit files, I recommend Audacity with the LAME plugin.

Copyright (C) 2017 Gloin Interactive, LLC

This program is free software: you can redistribute it and/or modify it under terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.

Version 1.0

# How do I get setup?
 - Clone this project
 - Enter the audio_trigger directory
 - Run install.sh
 - When it completes, it will reboot your device (by typing 
 ```
 ./install.sh
 ```
 )
 - After the reboot, run install_2.sh (by typing 
 ```
 ./install_2.sh
 ```
 )
 - Copy any relavent mp3's (or other audio files) to the media folder
 - Setup settings.xml for each audio file to be played (the program audio_editor.py can hlep create and edit the settings.xml file. It is run by typing 
 ```
 python audio_editor.py
 ```
 ).
 
# Additional Notes
 - The time setting in the settings.xml file should be rounded to the nearest tenth of a second
 - The time setting keeps the audio file from being triggered while it is still being played (mabye some day I'll automate this)
 - The switch setting is for dealing with triggers that are set to be normally open or normally closed
 
# To start the application:
 - type 
 ```
 python audio_trigger.py
 ```

# To have the application autostart:
 - Run 
 ```
 sudo crontab -e
 ```
 - Choose your favorite editor
 - Add the following to the bottom of the file:
 ```
 @reboot sh /home/pi/audio_trigger/launcher.sh
 ```
 - Save the file
 - Reboot the device
 - The auto playing will work whether booting to the terminal or the GUI

# Future Work
 - Add ability to have an audio file start automatically, and play continuously looped
 - Add ability for system to detect file play length dynamically

Kimball Willard  
<mailto:kimballw@gloininteractive.com>
