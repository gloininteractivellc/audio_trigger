#!/usr/bin/python

import getopt
from mutagen.mp3 import MP3
import sys

def main(argv):
    inputfile = ''
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile="])
    except getopt.GetoptError:
        print 'length.py -i <audio file>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'length.py -i <audio file>'
            sys.exit()
        elif opt in ("-i", "--infile"):
            inputfile = arg
    print 'Input file is ' + inputfile
    try:
        audio = MP3(inputfile)
        print("Length is " + str(audio.info.length))
    except Exception, e:
        print('There was an error: ' + str(e))
        sys.exit(0)

if __name__ == '__main__':
    main(sys.argv[1:])
